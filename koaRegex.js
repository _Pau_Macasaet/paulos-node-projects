import koa from 'koa';
import Router from 'koa-router';
const app = new koa()
const router = new Router()

const port = 4000

router.get('/number/:id([0-9]{5})', ctx => {
    // strictly numbers from 0 to 9
    // exactly 5 numbers
    console.log(ctx)
    const id = ctx.params.id
    const data = `Number: ${id}`
    ctx.status = 200
    ctx.body = data
})

router.get('/mixed/:mix([a-z0-9]{5})', ctx => {
    console.log(ctx)
    const id = ctx.params.mix
    const data = `Mixed Value ${id}`
    ctx.status = 200
    ctx.body = data
})

app.use(router.routes())
app.listen(port, () => {
    console.log('run port 4000')
})