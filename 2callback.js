function main() {
    console.log('Start') 
    setTimeout(() => console.log("Now we're finished"), 6000) 
    runWhileLoopForNSeconds(3) 
    // blocking code
    console.log('Finished, maybe') 
    setTimeout(() => console.log("Just kidding"), 4000)
}

function runWhileLoopForNSeconds(sec) {
    let start = Date.now(), now = start
    while (now - start < (sec*1000)) { 
        now = Date.now()
    }
    console.log('Time elapsed')
}

main()

// print 'start', pop out of stack
// setTimeout 1 to webAPI, timer starts
// print 'time elapsed' @ 3 secs
// print 'Finished maybe', stack now empty after pop
// setTimeout 2 to webAPI, timer starts
// setTimeout 1 timer ends @ 6 secs, pushed to task queue
// setTimeout 2 still at webAPI countdown
// setTimeout 1 enters call stack once empty, print "Now we're finished"
// setTimeout 2 timer ends @ 4 secs, pushed to task queue, then call stack once empty
// prints 'Just kidding' 1 sec after setTimeout 1 prints
// END