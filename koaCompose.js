import koa from 'koa';
const app = new koa()
import compose from 'koa-compose';

const port = 4000

async function random(ctx, next) {
    if('/random' == ctx.path) {
        ctx.body = Math.floor(Math.random() * 10)
    } else {
        await next()
    }
}

async function backwards(ctx, next) {
    if('/backwards' == ctx.path) {
        ctx.body = 'sdrawcab'
    } else {
        await next()
    }
}

async function pi(ctx, next) {
    if('/pi' == ctx.path) {
        ctx.body = String(Math.PI)
    } else {
        await next()
    }
}

const all = compose([random, backwards, pi])

app.use(all)

app.listen(port, console.log(`app listening on ${port}`))