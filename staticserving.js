import koa from 'koa';
import serve from 'koa-static-server';
const app = new koa()

const hostname = 'localhost'
const port = 3000

// connect to frontend
// serve simple, static assets to browser
app.use(serve({rootDir:'public'}))

app.listen(port, hostname, () => {
    console.log(`Server at http://${hostname}:${port}`)
})