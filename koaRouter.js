import koa from 'koa';
import Router from 'koa-router';
const app = new koa()
const router = new Router()

// GET method
router.get('/hello', ctx => {
    const data = 'hello world get'
    ctx.status = 200
    ctx.body = data
})

let PORT = 4000

app.use(router.routes())
app.listen(PORT, console.log(`port ${PORT}`))