//const { EventEmitter } = require('events')
import { EventEmitter } from "events";
const eventEmitter = new EventEmitter()

// handle
const eventHandler = function eventFunc() {
    console.log('event')
    eventEmitter.emit('lunch')
}

eventEmitter.on('lunch', () => {
    console.log('yummy')
    eventEmitter.emit('dinner')
})

eventEmitter.on('dinner', () => {
    console.log('dinner is steak')
})

// emit
// eventEmitter.emit('lunch')
// eventEmitter.emit('dinner')

eventHandler()