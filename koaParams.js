import koa from 'koa';
import Router from 'koa-router';
const app = new koa()
const router = new Router()

router.get('/hello/:name', ctx => {
    const name = ctx.params.name
    const data = `Hello ${name}`
    ctx.status = 200
    ctx.body = data
})

app.use(router.routes())
app.listen(4000, () => {
    console.log('run port 4000')
})