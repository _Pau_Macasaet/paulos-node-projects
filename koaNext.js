import koa from 'koa';
const app = new koa()

app.use(async (ctx, next) => {
    console.log('>> one')
    await next()
    console.log('>> ???')
})

app.use(async (ctx, next) => {
    console.log('>> two')
    ctx.body = 'two'
    await next() // add this to run next function
})

app.use(async (ctx, next) => {
    console.log('>> three')
    await next()
})

const hostname = 'localhost'
const port = 3000

app.listen(port, hostname, () => {
    console.log(`Server at http://${hostname}:${port}`)
})