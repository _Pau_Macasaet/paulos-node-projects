
const Koa = require('koa')
const fs = require('fs')
const app = new Koa()
const path = require('path')
const extname = path.extname

// try GET /app.js

app.use(async ctx => {
    const fpath = path.join(__dirname, ctx.path)
    const fstat = await stat(fpath)

    if (fstat.isFile()) {
        ctx.type = extname(fpath)
        ctx.body = fs.createReadStream(fpath)
    }
})

app.listen(3000)

function stat(file) {
    return new Promise((resolve, reject) => {
        fs.stat(file, (err, stat) => {
            if (err) reject(err)
            else resolve(stat)
        })
    })
}