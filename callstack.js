function firstFunction() {
    console.log('hello from first')
}

function secondFunction() {
    firstFunction()
    console.log('the end from second')
}

secondFunction()

// 1. When secondFunction() gets executed, an empty stack frame is created. It is the main (anonymous) entry point of the program.
// 2. secondFunction() then calls firstFunction()which is pushed into the stack.
// 3. firstFunction() returns and prints “Hello from firstFunction” to the console.
// 4. firstFunction() is pop off the stack.
// 5. The execution order then move to secondFunction().
// 6. secondFunction() returns and print “The end from secondFunction” to the console.
// 7. secondFunction() is pop off the stack, clearing the memory.

/*
When a function is invoked (called), the function, its parameters, 
and variables are pushed into the call stack to form a stack frame. 
This stack frame is a memory location in the stack. 
The memory is cleared when the function returns 
as it is pop out of the stack.
*/


function multiply(a, b) {
    return a * b
}

function square(n) {
    return multiply(n, n)
}

function printSquare(n) {
    let squared = square(n)
    console.log(squared)
}

printSquare(4)