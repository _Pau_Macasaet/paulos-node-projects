function c(str) {
    return `${str} zeta`
}

function b(){
    return c('bar')
}

function a() {
    const dev = b()
    console.log(`foo ${dev}`)
}
a() 

// call stack order async
console.log('foo') // 1
setTimeout(() => {
    console.log('zeta')
}, 2000) // 3 when stack clears
console.log('bar') // 2