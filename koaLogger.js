// nice to log things since you cant see to begin

import koa from 'koa';
const app = new koa()

// logger func

app.use(async (ctx, next) => {
    const start = Date.now()
    await next()
    const ms = Date.now() - start
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)

})

// next function: req/res

app.use(async ctx => {
    console.log(ctx.request)
    console.log(ctx.response)
    ctx.body = 'hello world this is logger'
})

app.listen(4000)