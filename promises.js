//const { readFile } = require('fs').promises
import { readFile } from "fs"

async function hello() {
    await readFile('./hello.txt', 'UTF8', (err, txt) => {
        console.log(`Awaited ${txt}`)
    })
}

hello()


console.log('start')

setTimeout(function() {
    console.log('timeout finished')
}, 0) 

Promise.resolve()
    .then(function() {
        console.log('promise 1')
    })
    .then(function() {
        console.log('promise 2')
    })
console.log('end')

/*
You can see how timeout finished is written at the end, 
because the script itself is treated as a macrotask 
so that at the end the enqueued microtasks are 
executed, that is, the two console.log 
referring to the callbacks of the promises.
*/