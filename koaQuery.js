import koa  from 'koa'
import Router  from 'koa-router'
const app = new koa()
const router = new Router()

const port = 4000

// localhost:4000/hello?name=anything
router.get('/hello', ctx => {
    
    console.log(ctx)
    const name = ctx.query.name
    const data = `name: ${name}`
    ctx.status = 200
    ctx.body = data
})

app.use(router.routes())
app.listen(port, () => {
    console.log('run port 4000')
})