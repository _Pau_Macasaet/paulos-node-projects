// Update with your config settings.
const path = require('path')
const dotenv = require('dotenv-safe')
dotenv.config()
const BASE_PATH = path.join(__dirname, 'src', 'server', 'db')

module.exports = {
  test: {
    client: 'pg',
    connection: `${process.env.DB_USERNAME}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@localhost:5432/koa_api_test`,
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }
  }, 
  development: {
    client: 'pg',
    connection: `${process.env.DB_USERNAME}://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@localhost:5432/koa_api`,
    migrations: {
      directory: path.join(BASE_PATH, 'migrations')
    },
    seeds: {
      directory: path.join(BASE_PATH, 'seeds')
    }

  }
}
