const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const session = require('koa-session')
const passport = require('koa-passport')
const RedisStore = require('koa-redis')

const indexRoutes = require('./routes/index')
const movieRoutes = require('./routes/movies')
const authRoutes = require('./routes/auth')

const app = new Koa()
const PORT = 1337

// sessions
app.keys = ['super-secret-key']
// app.use(session({
//     store: new RedisStore()
// }, app))
app.use(session(app))

app.use(bodyParser())

// authentication
require('./auth')
app.use(passport.initialize())
app.use(passport.session())

app.use(indexRoutes.routes())
app.use(movieRoutes.routes())
app.use(authRoutes.routes())

app.use(async ctx => {
    ctx.body = {
        status: 'success',
        message: 'hello, world!'
    }
})

const server = app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})

module.exports = server