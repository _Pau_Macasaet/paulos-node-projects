import Koa from 'koa';
import koaBody from 'koa-body';
const app = new Koa()

app.use(koaBody({
    jsonLimit: '1kb'
}))

// POST .name to /uppercase
// co-body accepts application/json
// and application/x-www-form-urlencoded

app.use(async ctx => {
    const body = ctx.request.body
    if (!body.name) ctx.throw(400, '.name required')
    ctx.body = { name: body.name.toUpperCase() }
})

app.listen(3000, console.log('running 3000'))